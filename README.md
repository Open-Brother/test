# test

## 介绍
本仓库存放开源大师兄项目的测试框架代码。

或者，使用经过整理的测试框架代码，见 https://gitee.com/liangkzgitee/TestFwk.git 

## 使用

将本仓库代码或者上面的 TestFwk 仓库的代码通过 git 命令下载到项目的 //applications/sample/OpenBrother/ 目录下，然后用文本编辑器打开 //applications/sample/BUILD.gn 文件，根据当前实际需要编译的项目来打开对应的编译目标，如下：

```
lite_component("app") {
     "OpenBrother:app",  # 编译开源大师兄的固件，打开这个编译目标
  ]
}
```

比如打开编译开源大师兄的固件的编译目标后，可以再进入 OpenBrother 子目录，打开其中的BUILD.gn文件，进一步打开要编译的目标，如下：

```
lite_component("app") {
  features = [       # 注意：下面两个编译目标同时打开会提示编译异常
    "TestFwk:app",   # 编译开源大师兄的工厂测试固件，打开这个编译目标
#   "pzstudio:app",  # 编译开源大师兄的出厂固件(带Python功能)，打开这个编译目标
#   "PzCar:pzcar_demo",  # 编译开源大师兄的PzCar项目固件，打开这个编译目标
  ]
}
```

更详细的使用说明，见 [README.md](https://gitee.com/Open-Brother/oh4ob/blob/master/README.md)