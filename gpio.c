/*
 * Copyright (C) 2021 HiHope Open Source Organization .
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 *
 * limitations under the License.
 */

#include <stdio.h>
#include <unistd.h>

#include "ohos_init.h"
#include "cmsis_os2.h"

#include "iot_gpio.h"
#include "hi_gpio.h"
#include "hi_io.h"
#include "hi_adc.h"
#include "hi_errno.h"


static void GpioTask(void *arg){
    (void)arg;
    IotGpioValue id_status;
    while (1)
    {
        IoTGpioGetInputVal(12, &id_status);
        if (id_status == IOT_GPIO_VALUE0) {
            printf("There is sound\r\n");
        }
        else
        {
            printf("No sound\r\n");
        }
    }
}


static void GpioEntry(void){
    printf("GMI Test!\n");
    osThreadAttr_t attr;

    // hi_gpio_init();
    // hi_io_set_func(HI_GPIO_IDX_12, HI_IO_FUNC_GPIO_12_GPIO);
    // hi_gpio_set_dir(HI_GPIO_IDX_12, HI_GPIO_DIR_IN);


    attr.name = "GpioTask";
    attr.attr_bits = 0U;
    attr.cb_mem = NULL;
    attr.cb_size = 0U;
    attr.stack_mem = NULL;
    attr.stack_size = 1024;
    attr.priority = 25;

    if (osThreadNew(GpioTask, NULL, &attr) == NULL) {
        printf("[LedExample] Falied to create LedTask!\n");
    }
}

SYS_RUN(GpioEntry);