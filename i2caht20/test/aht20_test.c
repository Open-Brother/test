/*
 * Copyright (C) 2021 HiHope Open Source Organization .
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 *
 * limitations under the License.
 */

#include "aht20.h"

#include <stdio.h>
#include <unistd.h>

#include "ohos_init.h"
#include "cmsis_os2.h"
#include "hi_gpio.h"
#include "hi_io.h"
#include "hi_i2c.h"

#include "ssd1306.h"
#include "ssd1306_tests.h"

void Aht20TestTask(void* arg)
{
    char str[50];
    (void) arg;
    uint32_t retval = 0;
/*
    hi_io_set_func(HI_IO_NAME_GPIO_13, HI_IO_FUNC_GPIO_13_I2C0_SDA);
    hi_io_set_func(HI_IO_NAME_GPIO_14, HI_IO_FUNC_GPIO_14_I2C0_SCL);

    hi_i2c_init(HI_I2C_IDX_0, 400*1000);
*/
    retval = AHT20_Calibrate();
    printf("AHT20_Calibrate: %d\r\n", retval);


    float temp = 0.0, humi = 0.0;

    retval = AHT20_StartMeasure();
    printf("AHT20_StartMeasure: %d\r\n", retval);

    retval = AHT20_GetMeasureResult(&temp, &humi);
    printf("AHT20_GetMeasureResult: %d, temp = %.2f, humi = %.2f\r\n", retval, temp, humi);

    if(HI_ERR_SUCCESS == retval)
    {
        snprintf(str, sizeof(str), "%.2f AHT20 OK", temp, humi);
    }else{
        snprintf(str, sizeof(str), "AHT20 ERROR", temp, humi);
    }
    

    ssd1306_SetCursor(0,0);
    ssd1306_DrawString(str, Font_7x10, White);
}



