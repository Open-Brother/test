/*
 * Copyright (C) 2021 HiHope Open Source Organization .
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 *
 * limitations under the License.
 */
/*
 * io_oled_demo.h
 *
 */

#ifndef IO_OLED_DEMO_H_
#define IO_OLED_DEMO_H_

typedef enum {
    /** Other */
    OTHER_TEST = 0,
    /** STA */
    ATH_TEST,
    /** AP */
    AP_TEST,
    /** nfc */
    NFC_TEST,
    /** unisound */
    UNI_TEST
} TestMode;


#endif /* IO_OLED_DEMO_H_ */