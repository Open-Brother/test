/*
 * Copyright (C) 2021 HiHope Open Source Organization .
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 *
 * limitations under the License.
 */

#include "lis3dh_driver.h"

#include <stdio.h>
#include <unistd.h>

#include "ohos_init.h"
#include "cmsis_os2.h"
#include "hi_gpio.h"
#include "hi_io.h"
#include "hi_i2c.h"



#include "ssd1306.h"
#include "ssd1306_tests.h"


#define __EXAMPLE1__H  1

//#define __EXAMPLE2__H  1


void Lis3df_test(void)
{
	char str[50];
	uint8_t buffer[26];
	uint8_t imu_id = 0;
	int response = 1;
	uint8_t position=0, old_position=0;
	AxesRaw_t data;
	//Inizialize MEMS Sensor
	//set ODR (turn ON device)
	LIS3DH_GetWHO_AM_I(&imu_id);
	if(imu_id!=0x33){
			printf("id error %d\r\n",imu_id);
			return ;
	}else{
			printf("id = %d\r\n",imu_id);
			
	}

	response = LIS3DH_SetODR(LIS3DH_ODR_100Hz);
	if(response==MEMS_SUCCESS){
			printf("\n\rSET_ODR_OK    \n\r\0");
			
	}else{
			printf("\n\rSET_ODR_FAILED    \n\r\0");
	}
	//set PowerMode 
	response = LIS3DH_SetMode(LIS3DH_NORMAL);
	if(response==MEMS_SUCCESS){
			printf("SET_MODE_OK     \n\r\0");
			
	}
	//set Fullscale
	response = LIS3DH_SetFullScale(LIS3DH_FULLSCALE_2);
	if(response==MEMS_SUCCESS){
			printf("SET_FULLSCALE_OK\n\r\0");
			
	}
	//set axis Enable
	response = LIS3DH_SetAxis(LIS3DH_X_ENABLE | LIS3DH_Y_ENABLE | LIS3DH_Z_ENABLE);
	if(response==MEMS_SUCCESS){
			printf("SET_AXIS_OK     \n\r\0");
		
	}

	//get Acceleration Raw data  
	response = LIS3DH_GetAccAxesRaw(&data);
	if(response==MEMS_SUCCESS){
		//print data values
		//function for MKI109V1 board 
		printf("X=%6d Y=%6d Z=%6d \r\n", data.AXIS_X, data.AXIS_Y, data.AXIS_Z);
		old_position = position;

		snprintf(str, sizeof(str), "3 axis OK!");
		ssd1306_SetCursor(0,30);
        ssd1306_DrawString(str, Font_7x10, White);
	}else{
		snprintf(str, sizeof(str), "3 axis ERROR!");
		ssd1306_SetCursor(0,30);
        ssd1306_DrawString(str, Font_7x10, White);
	}
}
#if 0
void Lis3dh_Example(void){
  uint8_t buffer[26];
  uint8_t imu_id = 0;
  int response = 1;
  uint8_t position=0, old_position=0;
  AxesRaw_t data;
  //Inizialize MEMS Sensor
  //set ODR (turn ON device)
  LIS3DH_GetWHO_AM_I(&imu_id);
  if(imu_id!=0x33){
  		printf("id error %d\r\n",imu_id);
		return ;
  }else{
		printf("id = %d\r\n",imu_id);
		
  }
  response = LIS3DH_SetODR(LIS3DH_ODR_100Hz);
  if(response==MEMS_SUCCESS){
        printf("\n\rSET_ODR_OK    \n\r\0");
        
  }else{
		printf("\n\rSET_ODR_FAILED    \n\r\0");
  }
  //set PowerMode 
  response = LIS3DH_SetMode(LIS3DH_NORMAL);
  if(response==MEMS_SUCCESS){
        printf("SET_MODE_OK     \n\r\0");
        
  }
  //set Fullscale
  response = LIS3DH_SetFullScale(LIS3DH_FULLSCALE_2);
  if(response==MEMS_SUCCESS){
        printf("SET_FULLSCALE_OK\n\r\0");
        
  }
  //set axis Enable
  response = LIS3DH_SetAxis(LIS3DH_X_ENABLE | LIS3DH_Y_ENABLE | LIS3DH_Z_ENABLE);
  if(response==MEMS_SUCCESS){
        printf("SET_AXIS_OK     \n\r\0");
       
  }

  
/*********************/  
/******Example 1******/ 
#ifdef __EXAMPLE1__H 
	while(1){
		//get Acceleration Raw data  
		response = LIS3DH_GetAccAxesRaw(&data);
		if(response==MEMS_SUCCESS){
			//print data values
			//function for MKI109V1 board 
			printf("X=%6d Y=%6d Z=%6d \r\n", data.AXIS_X, data.AXIS_Y, data.AXIS_Z);
			old_position = position;

		}
		osDelay(1);
	}
#endif /* __EXAMPLE1__H  */ 
 
 
/*********************/
/******Example 2******/
#ifdef __EXAMPLE2__H
	//configure Mems Sensor
	//set Interrupt Threshold 
	response = LIS3DH_SetInt1Threshold(20);
	if(response==MEMS_SUCCESS){
	    printf("SET_THRESHOLD_OK\n\r\0");
	}
	//set Interrupt configuration (all enabled)
	response = LIS3DH_SetIntConfiguration(LIS3DH_INT1_ZHIE_ENABLE | LIS3DH_INT1_ZLIE_ENABLE |
									   LIS3DH_INT1_YHIE_ENABLE | LIS3DH_INT1_YLIE_ENABLE |
									   LIS3DH_INT1_XHIE_ENABLE | LIS3DH_INT1_XLIE_ENABLE ); 
	if(response==MEMS_SUCCESS){
		printf("SET_INT_CONF_OK \n\r\0");
	    
	}
	//set Interrupt Mode
	response = LIS3DH_SetIntMode(LIS3DH_INT_MODE_6D_POSITION);
	if(response==MEMS_SUCCESS){
	    printf("SET_INT_MODE    \n\r\0");
	    
	}

	while(1){
		//get 6D Position
		response = LIS3DH_Get6DPosition(&position);
		if((response==MEMS_SUCCESS) && (old_position!=position)){
			switch (position){
				case LIS3DH_UP_SX:   printf("\n\rposition = UP_SX  \n\r\0");   break;
				case LIS3DH_UP_DX:   printf("\n\rposition = UP_DX  \n\r\0");   break;
				case LIS3DH_DW_SX:   printf("\n\rposition = DW_SX  \n\r\0");   break;              
				case LIS3DH_DW_DX:   printf("\n\rposition = DW_DX  \n\r\0");   break; 
				case LIS3DH_TOP:     printf("\n\rposition = TOP    \n\r\0");   break; 
				case LIS3DH_BOTTOM:  printf("\n\rposition = BOTTOM \n\r\0");   break; 
				default:             printf("\n\rposition = unknown\n\r\0");   break;
			}

			//function for MKI109V1 board   
			old_position = position;
		}
		osDelay(1);
	}
#endif /*__EXAMPLE2__H */ 

}


void Lis3dhTestTask(void* arg)
{
    (void) arg;
    uint32_t retval = 0;

    hi_io_set_func(HI_IO_NAME_GPIO_13, HI_IO_FUNC_GPIO_13_I2C0_SDA);
    hi_io_set_func(HI_IO_NAME_GPIO_14, HI_IO_FUNC_GPIO_14_I2C0_SCL);

    hi_i2c_init(HI_I2C_IDX_0, 400*1000);
	Lis3dh_Example();
    
}

void Lis3dhTest(void)
{
    osThreadAttr_t attr;

    attr.name = "Aht20Task";
    attr.attr_bits = 0U;
    attr.cb_mem = NULL;
    attr.cb_size = 0U;
    attr.stack_mem = NULL;
    attr.stack_size = 4096;
    attr.priority = osPriorityNormal;

    if (osThreadNew(Lis3dhTestTask, NULL, &attr) == NULL) {
        printf("[Lis3dhTest] Failed to create Lis3dhTestTask!\n");
    }
}

#endif