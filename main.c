/*
 * Copyright (C) 2021 HiHope Open Source Organization .
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 *
 * limitations under the License.
 */
#include <stdio.h>
#include <unistd.h>

#include "ohos_init.h"
#include "cmsis_os2.h"
#include "iot_gpio.h"
#include "iot_errno.h"
#include "hi_pwm.h"
#include "hi_io.h"

#include "iot_gpio.h"
#include "hi_io.h"

#include "io_oled_demo.h"

#include "ssd1306.h"
#include "ssd1306_tests.h"

#include "lis3dh_driver.h"

#define BUTTON_A_PRESSED    1
#define BUTTON_B_PRESSED    2

int button_b = 0;
int button_a = 0;

int get_button_b(void)
{
    printf("_____>>>>>button_b is %d\r\n", button_b);
    return button_b;
}

int get_button_a(void)
{
    printf("_____>>>>>button_a is %d\r\n", button_a);
    return button_a;
}


void clean_button_b(void)
{
    printf("_____>>>>> %s %d \r\n", __FILE__, __LINE__);
    button_b == 0;
}

void clean_button_a(void)
{
    printf("_____>>>>> %s %d \r\n", __FILE__, __LINE__);
    button_a == 0;
}

static void OnButtonPressed_B(char *arg)
{
    printf("_____>>>>> %s %d \r\n", __FILE__, __LINE__);


    button_b = 1;
    
}


static void OnButtonPressed_A(char *arg)
{
    printf("_____>>>>> %s %d \r\n", __FILE__, __LINE__);


    button_a = 1;
}

int get_button_status(void)
{
    int ret = 0;
    while(1)
    {
        if(button_a == 1)
        {   
            usleep(500000);
            button_a = 0;
            clean_button_a();
            return BUTTON_A_PRESSED;
            break;
        }
        if(button_b == 1)
        {
            usleep(500000);
            button_b = 0;
            clean_button_b();
            return BUTTON_B_PRESSED;
            break;
        }
        //ssd1306_UpdateScreen();
        usleep(10000);
    }


}


void NFC_Task(void *arg)
{
    nfcTask();
}

void UART1_Task(void *arg)
{
    UartDemo_Task();
}

static void CreateNFC_UART_Task(void) 
{ 
    osThreadAttr_t attr = {0}; 
 

    attr.name = "NFC_Task"; 
    attr.attr_bits = 0U; 
    attr.cb_mem = NULL; 
    attr.cb_size = 0U; 
    attr.stack_mem = NULL; 
    attr.stack_size = 10240;//堆栈大小 
    attr.priority = osPriorityNormal;//优先�?? 
 
    if (osThreadNew((osThreadFunc_t)NFC_Task, NULL, &attr) == NULL) 
    { 
        printf("[CreateNFC_UART_Task] Falied to create NFC_Task!\n"); 
    } 

    osThreadAttr_t attr1 = {0}; 


    attr1.name = "UART1_Task"; 
    attr1.attr_bits = 0U; 
    attr1.cb_mem = NULL; 
    attr1.cb_size = 0U; 
    attr1.stack_mem = NULL; 
    attr1.stack_size = 1024*4;//堆栈大小 
    attr1.priority = osPriorityNormal;//优先�?? 
 
    if (osThreadNew((osThreadFunc_t)UART1_Task, NULL, &attr1) == NULL) 
    { 
        printf("[CreateNFC_UART_Task] Falied to create UART1_Task!\n"); 
    } 
} 

int g_modeStatus = OTHER_TEST;

#define OLED_I2C_BAUDRATE 400*1000

#define     IOT_PWM_PORT_PWM2   2
#define     IOT_PWM_BEEP        5


static void MainDemo_Task(void *arg)
{
    (void)arg;
    
    hi_watchdog_disable();
    
    IoTGpioInit(2);
    hi_io_set_func(2, 0);
    IoTGpioSetDir(2, IOT_GPIO_DIR_IN);
    hi_io_set_pull(2, 1);

    IoTGpioRegisterIsrFunc(2, IOT_INT_TYPE_EDGE, IOT_GPIO_EDGE_FALL_LEVEL_LOW,
        OnButtonPressed_B, NULL);

    IoTGpioInit(10);
    hi_io_set_func(10, 0);
    IoTGpioSetDir(10, IOT_GPIO_DIR_IN);
    hi_io_set_pull(10, 1);

    IoTGpioRegisterIsrFunc(10, IOT_INT_TYPE_EDGE, IOT_GPIO_EDGE_FALL_LEVEL_LOW,
        OnButtonPressed_A, NULL);


    IoTGpioInit(HI_IO_NAME_GPIO_13);
    IoTGpioInit(HI_IO_NAME_GPIO_14);

    hi_io_set_func(HI_IO_NAME_GPIO_13, HI_IO_FUNC_GPIO_13_I2C0_SDA);
    hi_io_set_func(HI_IO_NAME_GPIO_14, HI_IO_FUNC_GPIO_14_I2C0_SCL);
    
    IoTI2cInit(0, OLED_I2C_BAUDRATE);

    //WatchDogDisable();

    printf("____>>>>>> %s %d \r\n", __FILE__, __LINE__);
    usleep(20*1000);
    ssd1306_Init();
    

    CreateNFC_UART_Task();


    IoTGpioInit(IOT_PWM_BEEP);
    hi_io_set_func(IOT_PWM_BEEP, 5);
    IoTGpioSetDir(IOT_PWM_BEEP, IOT_GPIO_DIR_OUT);
    IoTPwmInit(IOT_PWM_PORT_PWM2);

#if 0
    while(1)
    {
        g_modeStatus = NFC_TEST;
        usleep(20*1000);
    }
#endif
    while(1)
    {
MainDemo_1:

        printf("_____>>>>>> MainDemo_1\r\n");

        ssd1306_Fill(White);
        ssd1306_UpdateScreen();
        
        StartPWMBeerTask();

        //IoTPwmStart(2);

        IoTPwmStop(2);

        ssd1306_UpdateScreen();

        if(get_button_status() == BUTTON_A_PRESSED)
            goto MainDemo_5;

MainDemo_2: 
        printf("_____>>>>>> MainDemo_2\r\n");

        IoTPwmStart(2, 50, 4000);

        ssd1306_Fill(Black);

        //IoTPwmStart(2);

        Aht20TestTask(NULL);
        Ltr553Test();
        //SixAxisTask();
        Lis3df_test();

        ssd1306_UpdateScreen();

        if(get_button_status() == BUTTON_A_PRESSED)
            goto MainDemo_1;

MainDemo_3: 
        printf("_____>>>>>> MainDemo_3\r\n");
        
        IoTPwmStop(2);

        ssd1306_Fill(Black);

        StopHotspot();
        WifiHotspotTask(NULL);

        ssd1306_UpdateScreen();

        if(get_button_status() == BUTTON_A_PRESSED)
            goto MainDemo_2;

MainDemo_4: 
        printf("_____>>>>>> MainDemo_4\r\n");
        
        ssd1306_Fill(Black);

        ssd1306_SetCursor(0,0);
        ssd1306_DrawString("NFC TEST...", Font_7x10, White);
        ssd1306_UpdateScreen();

        g_modeStatus = NFC_TEST;

        if(get_button_status() == BUTTON_A_PRESSED)
        {
            g_modeStatus = OTHER_TEST;
            goto MainDemo_3;
        }

        g_modeStatus = OTHER_TEST;

MainDemo_5: 
        printf("_____>>>>>> MainDemo_5\r\n");

        ssd1306_Fill(Black);

        ssd1306_SetCursor(0,0);
        ssd1306_DrawString("UNI TEST... ", Font_7x10, White);
        ssd1306_SetCursor(0,15);
        ssd1306_DrawString("wait... ", Font_7x10, White);

        ssd1306_UpdateScreen();
        
        if(get_button_status() == BUTTON_A_PRESSED)
            goto MainDemo_4;
    }
}



static void Main_Entry(void) 
{ 
    osThreadAttr_t attr = {0}; 
 
    printf("[Main_Entry] MainDemo_Task()\n"); 
 
    attr.name = "MainDemo_Task"; 
    attr.attr_bits = 0U; 
    attr.cb_mem = NULL; 
    attr.cb_size = 0U; 
    attr.stack_mem = NULL; 
    attr.stack_size = 10240;//堆栈大小 
    attr.priority = osPriorityNormal;//优先�?? 
 
    if (osThreadNew((osThreadFunc_t)MainDemo_Task, NULL, &attr) == NULL) 
    { 
        printf("[Main_Entry] Falied to create MainDemo_Task!\n"); 
    } 
} 
 


//3.注册模块 
SYS_RUN(Main_Entry); 
