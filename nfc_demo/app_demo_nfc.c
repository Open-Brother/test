/*
 * Copyright (C) 2021 HiHope Open Source Organization .
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 *
 * limitations under the License.
 */
#include "iot_i2c.h"
#include "ohos_init.h"
#include "cmsis_os2.h"
#include "iot_gpio.h"
// #include "iot_gpio_ex.h"
#include <unistd.h>
#include "app_demo_config.h"
#include "io_oled_demo.h"


unsigned int g_c08i_nfc_demo_task_id = 0;
unsigned int g_nfc_display_task_id = 0;
extern unsigned int g_mux_id;
unsigned char read_reg =0;
extern unsigned char nfc_mode;

#define HI_I2C_IDX_BAUDRATE         400000//400k
/*i2c read*/
unsigned int write_read(unsigned char reg_high_8bit_cmd, unsigned char reg_low_8bit_cmd, unsigned char* recv_data, unsigned char send_len, unsigned char read_len)
{
    unsigned int ret;

    IotI2cIdx id = IOT_I2C_IDX_0;
    IotI2cData co8i_nfc_i2c_read_data ={0};
    IotI2cData c081nfc_i2c_write_cmd_addr ={0};

    unsigned char _send_user_cmd[2] = {reg_high_8bit_cmd, reg_low_8bit_cmd};

    memset(recv_data, 0x0, sizeof(recv_data));
    memset(&co8i_nfc_i2c_read_data, 0x0, sizeof(IotI2cData));

    c081nfc_i2c_write_cmd_addr.sendBuf = _send_user_cmd;
    c081nfc_i2c_write_cmd_addr.sendLen = send_len;

    co8i_nfc_i2c_read_data.receiveBuf = recv_data;
    co8i_nfc_i2c_read_data.receiveLen = read_len;

    read_reg = NFC_CLEAN;//娑堥櫎stop淇″彿

    IoTI2cWrite(id, C081_NFC_ADDR&0xFE, c081nfc_i2c_write_cmd_addr.sendBuf, c081nfc_i2c_write_cmd_addr.sendLen);
    IoTI2cRead(id, C081_NFC_ADDR|I2C_RD, co8i_nfc_i2c_read_data.receiveBuf, co8i_nfc_i2c_read_data.receiveLen);

    return 0;
}


/* NFC 鑺墖閰嶇疆 ,骞虫椂涓嶈璋冪�? NFC init*/
void nfc_init(void)
{
 	//閰嶇疆妯″紡
    nfc_mode = 0;
 	Protocol_Config();
    FM11_Init();
}

void nfcTask(void)
{
    
    IoTGpioInit(6);
    IoTGpioInit(8);
    IoTGpioSetOutputVal(6, IOT_GPIO_VALUE0);
    IoTGpioSetOutputVal(8, IOT_GPIO_VALUE1);
    
    IoTGpioInit(13);
    hi_io_set_func(13, 6);
    IoTGpioInit(14);
    hi_io_set_func(14, 6);

    IoTI2cInit(0, HI_I2C_IDX_BAUDRATE);//baud 400k
    IoTI2cSetBaudrate(0, HI_I2C_IDX_BAUDRATE);
    /*鏇存敼NFC鑺墖淇℃伅锛屽啓EEPROM ,骞虫椂涓嶈鎵撳�?*/
    nfc_init();
    printf("nfc task\r\n");
    while(1)
    {
        nfcread();
        usleep(10000);
    }
}
 