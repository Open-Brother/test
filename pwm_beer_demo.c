/*
 * Copyright (C) 2021 HiHope Open Source Organization .
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 *
 * limitations under the License.
 */

#include <stdio.h>
#include <unistd.h>

#include "ohos_init.h"
#include "cmsis_os2.h"
#include "iot_gpio.h"
#include "iot_errno.h"
#include "hi_pwm.h"
#include "hi_io.h"


void StartPWMBeerTask(void)
{
    osThreadAttr_t attr;

    IoTGpioInit(5);
    hi_io_set_func(5, HI_IO_FUNC_GPIO_5_PWM2_OUT);
    IoTGpioSetDir(5, IOT_GPIO_DIR_OUT);
    IoTPwmInit(2);

    IoTPwmStart(2, 50, 4000);
}
