/*
 * Copyright (C) 2021 HiHope Open Source Organization .
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 *
 * limitations under the License.
 */
#include <stdio.h>
#include <unistd.h> 
#include "ohos_init.h" 
#include "cmsis_os2.h" 
#include "iot_gpio.h" 
#include "iot_uart.h" 
#include "hi_io.h"
#include "io_oled_demo.h"
#include "ssd1306.h"
#include "ssd1306_tests.h"

#define		IOT_UART_IDX_2		 1
#define     IOT_PWM_PORT_PWM2   2
#define     IOT_PWM_BEEP        5
#define		LED_PWM		0


unsigned char uartWriteBuff[] = {"hello uart!"};
unsigned char uartReadBuff[16] = {0};
unsigned char Wake_Up[] = {0x5A, 0xA5, 0x64, 0x73, 0x78, 0x5A};
unsigned char Buzzer_On[] = {0x5A, 0xA5, 0x04, 0x64, 0x6b, 0x62, 0x6a, 0x5A};
unsigned char Buzzer_Off[] = {0x5A, 0xA5, 0x04, 0x67, 0x62, 0x62, 0x6a, 0x5A};
unsigned char LED_On[] = {0x5A, 0xA5, 0x02, 0x6F, 0x6E, 0x5A};
unsigned char LED_Off[] = {0x5A, 0xA5, 0x03, 0x6F, 0x66, 0x66, 0x5A};

int uartInitCfg = 0;

int usr_uart_config(void) 
{ 
    int ret; 
    IotUartAttribute g_uart_cfg = {115200, 8, 1, IOT_UART_PARITY_NONE, 500, 500, 0}; 
    ret = IoTUartInit(IOT_UART_IDX_2, &g_uart_cfg); 
    uartInitCfg = 1;
 
    if (ret != 0)  
    { 
        printf("uart init fail\r\n"); 
    }

    return ret; 
} 


int CMP_Array(unsigned char *Array_Local, unsigned char *Array_Remote, unsigned char num)

{
	for(unsigned char i = 0; i < num; i++ )
	{
        printf("444444444\r\n");
        
		if(*Array_Local == *Array_Remote)
		{
			Array_Local++;		
			Array_Remote++;
		}
		else
		{
			return false;		//否则返回错误
			
		}
	
	}
	return true;		//所有字符比较结束，返回正确

}
 
//1.任务处理函数 
void UartDemo_Task(void) 
{ 
    unsigned int len = 0; //串口接收数据长度
    char duty = 50;	
    printf("[UartDemo] UartDemo_Task()\n"); 
 
    IoTGpioInit(0);//使用GPIO，需要调用该接口 
    IoTGpioInit(1);//使用GPIO，需要调用该接口 
    hi_io_set_func(5,0);    
    hi_io_set_func(6,0);  
    hi_io_set_func(1,2);

    IoTGpioInit(IOT_PWM_BEEP);
    hi_io_set_func(IOT_PWM_BEEP, 5);
    IoTGpioSetDir(IOT_PWM_BEEP, IOT_GPIO_DIR_OUT);
    IoTPwmInit(IOT_PWM_PORT_PWM2);

    hi_io_set_func(0,0);  
    IoTGpioSetDir(0, IOT_GPIO_DIR_OUT);
    IoTGpioSetOutputVal(0, 0);

    if (uartInitCfg == 0){
        printf("UART init...\r\n"); 
        usr_uart_config(); 
    } 
    
    IoTUartWrite(IOT_UART_IDX_2, (unsigned char *)uartWriteBuff, sizeof(uartWriteBuff));

    while(1) 
    { 
        len = IoTUartRead(IOT_UART_IDX_2, uartReadBuff, sizeof(uartReadBuff));
        if (len > 0) {
            printf("3333333333333333\r\n");
            
                if(true == CMP_Array(&Buzzer_On, &uartReadBuff, sizeof(Buzzer_On)/sizeof(Buzzer_On[0]))){
                    printf("Get Buzzer_On CMD\r\n");
				    IoTPwmStart(IOT_PWM_PORT_PWM2, 50, 4000);
                    
                }
                else if(true == CMP_Array(&Buzzer_Off, &uartReadBuff, sizeof(Buzzer_Off)/sizeof(Buzzer_Off[0]))){
                    printf("Get Buzzer_Off CMD\r\n");
				    IoTPwmStop(IOT_PWM_PORT_PWM2);
                    
                }
                else if(true == CMP_Array(&Wake_Up, &uartReadBuff, sizeof(Wake_Up)/sizeof(Wake_Up[0]))){
                    printf("Get Wake_Up CMD\r\n");
                    
                    ssd1306_SetCursor(0,0);
                    ssd1306_DrawString("UNI TEST OK ", Font_7x10, White);
                    ssd1306_SetCursor(0,15);
                    ssd1306_DrawString("OK OK OK ", Font_7x10, White);

                    ssd1306_UpdateScreen();
                }
                else if(true == CMP_Array(&LED_On, &uartReadBuff, sizeof(LED_On)/sizeof(LED_On[0]))){
                    printf("Get LED_On CMD\r\n");
				    IoTGpioSetOutputVal(0, 1);
                    
                }
                else if(true == CMP_Array(&LED_Off, &uartReadBuff, sizeof(LED_Off)/sizeof(LED_Off[0]))){
                    printf("Get LED_Off CMD\r\n");
				    IoTGpioSetOutputVal(0, 0);
                    
                }
                
		     }	
        usleep(100000); 
    }
    return NULL; 
} 
 

